from time import sleep
from ROOT import *
import csv
import numpy as np

txdiff_increments = {390 : 0  ,530 : 1 , 650 : 2, 850 : 3}
eq_increments = {0 : 0 , 3: 4, 6: 8}
tx_post_increments = {0: 0 , 2.98 : 3, 5.81 : 6}
tx_pre_increments = {0.01 : 0, 3.08 : 1, 5.67 : 2}

def MakePalette():
    stops =  np.array([0.5,1],dtype=float)
    red =  np.array([0,1],dtype=float)
    green=  np.array([1,0],dtype=float)
    blue =  np.array([0,0],dtype=float)
    FI = TColor.CreateGradientColorTable(2, stops,red,green,blue, 120)


def ReadData(filename): 
# This function extract the wanted data from the CSV file I made from your spreadsheet
    data = []
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for i,row in enumerate(spamreader):
            if i!=0:
                d=[int(row[0]),float(row[1]),float(row[2]),int(row[3]),int(row[4])]
                data.append(d)
                print(d)

    return data

#This function create unique bin number (X,Y) for each data points
def GenerateBins(data):
    bins = [] 
    for d in data : 
        x= txdiff_increments[d[0]] + eq_increments[d[3]]
        y= tx_pre_increments[d[1]] + tx_post_increments[d[2]]
        bins.append([x,y])
    return bins


#We read the data from Data.csv, then generate the bin numbers 
data = ReadData("Data2.csv")
bins = GenerateBins(data)

# We create the ROOT histogram
histo = TH2I("h","Number of bad channels",12,-0.5,11.5,9,-0.5,8.5)

# We fill the histogram using the bin numbers 
for i,d in enumerate(data):
    histo.Fill(bins[i][0],bins[i][1],d[4])

#We modify the X axis labels to reflect the real labels we want 
for i in range(12):
    if i+1<=4:
        histo.GetXaxis().SetBinLabel(i+1,"0")
    elif i+1<=8:
        histo.GetXaxis().SetBinLabel(i+1,"3")
    elif i+1<=12:
        histo.GetXaxis().SetBinLabel(i+1,"6")
histo.GetXaxis().SetTitle("FF Tx Eq. (dB)")
histo.GetYaxis().SetTitle("Pre-cursor (dB)")
histo.GetZaxis().SetTitle("Number of bad channels (BER > 1e-13)")

#We modify the Y axis labels to reflect the real labels we want 
for i in range(9):
    if (i+1)%3==1:
        histo.GetYaxis().SetBinLabel(i+1,"0.01 dB")
    elif (i+1)%3==2:
        histo.GetYaxis().SetBinLabel(i+1,"3.08 dB")
    elif (i+1)%3==0:
        histo.GetYaxis().SetBinLabel(i+1,"5.67 dB")




gStyle.SetPalette(108)
gStyle.SetPalette(55)
MakePalette()
gStyle.SetHistMinimumZero()

gStyle.SetOptTitle(0)
#Create a root file to save the results 
f = TFile("result.root","recreate")

# We plot the histogram and put it on a canvas
can = TCanvas()
histo.Draw("colztext0")
histo.SetStats(0)
histo.SetMinimum(-1)

# We add the second X axis 
sec_x_axis = TGaxis(-0.5,8.5,11.5,8.5,-0.5,11.5,12,"-")
sec_x_axis.Draw()
sec_x_axis.SetLabelSize(0.02)
sec_x_axis.SetTitle("Tx Swing (mV)")
sec_x_axis.SetLabelFont(42)
sec_x_axis.SetTitleFont(42)

# We change the labels for second X Axis
for i in range(12):
    if (i+1)%4==1:
        sec_x_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"390")
    elif (i+1)%4==2:
        sec_x_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"530")
    elif (i+1)%4==3:
        sec_x_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"650")
    elif (i+1)%4==0:
        sec_x_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"850")

# We add the second Y axis 
sec_y_axis = TGaxis(11.5,-0.5,11.5,8.5,-0.5,8.5,9,"+")
sec_y_axis.SetLabelOffset(0.03)
sec_y_axis.Draw()
sec_y_axis.SetLabelSize(0.02)
sec_y_axis.SetTitle("Post-cursor (dB)")
sec_y_axis.SetLabelFont(42)
sec_y_axis.SetTitleFont(42)

# We change the labels for second Y Axis
for i in range(9):
    if (i+1)<=3:
        sec_y_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"0")
    elif (i+1)<=6:
        sec_y_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"2.98")
    elif (i+1)<=9:
        sec_y_axis.ChangeLabel(i+1,-1,-1,-1,-1,-1,"5.81")

gStyle.SetHistMinimumZero()
gPad.SetRightMargin(0.2)
can.Update()

#sleep(5)
histo.GetListOfFunctions().FindObject("palette")
p=histo.GetListOfFunctions().FindObject("palette")

histo.GetListOfFunctions().FindObject("palette").SetX1NDC(0.89)
histo.GetListOfFunctions().FindObject("palette").SetX2NDC(0.92)
histo.GetListOfFunctions().FindObject("palette").SetY1NDC(0.1)
histo.GetListOfFunctions().FindObject("palette").SetY2NDC(0.8)
can.SetWindowSize(1800,1080)
can.Update()

# Write plot to ROOT file and close 
can.Write()
f.Close()

